# Network UPS Tools Ansible roles

## Getting started

1. Generate passwords for the NUT server users (`upsmon`, `admin`) using something like `pwgen`.

```bash
$ pwgen -s 40 1
```

2. Encrypt them using Ansible Vault and insert the generated cyphertexts into the password fields (`nut_client_password`, `nut_admin_password`) in `inventory/group_vars/all/nut.yml`.

```bash
$ ansible-vault encrypt_string
```

3. Change the instance-specific configuration according to your needs (`inventory/example_server/nut.yml`).\
   You may insert overrides for all global inventory variables here, such as `nut_server_listen` to change what addresses the NUT server listens to.

4. Integrate the role and inventory into your Ansible environment.

    * I wasn't able to confirm if you are able to specify multiple `quirks` parameters to the `usbkid` kernel module. Therefore, the USB HID quirk stuff probably should be in a common "setup all instances" role, with group/instance specific inventory variables controlling whether or not it gets used or not,

5. ???

6. Profit!
